EXE=dwmstat
GO_SRC=$(wildcard *.go)
# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

.PHONY: install clean

${EXE}: ${GO_SRC}
	go build -o $@

install: ${EXE} dwmstat.1
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${EXE} ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/${EXE}
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < dwmstat.1 > ${DESTDIR}${MANPREFIX}/man1/dwmstat.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/dwmstat.1

clean:
	rm -rf ${EXE}
